<?php 
include("connection.php");

$query = "SELECT * FROM profile";
$result = mysqli_query($db, $query) or die(mysqli_error($db));
$rows=mysqli_fetch_all($result);
?>
<html>
<head>
<style>
body{width:615px;font-family:arial;letter-spacing:1px;line-height:20px;}
.button_link {color:#FFF;text-decoration:none; background-color:#428a8e;padding:10px;}
.frm-add {border: #c3bebe 1px solid;
    padding: 30px;}
.demo-form-heading {margin-top:0px;font-weight: 500;}	
.demo-form-row{margin-top:20px;}
.demo-form-field{width:300px;padding:10px;}
.demo-form-submit{color:#FFF;background-color:#414444;padding:10px 50px;border:0px;cursor:pointer;}
</style>
</head>
<body>
<div class="frm-add">
<h1 class="demo-form-heading">Edit Record</h1>
<form name="frmAdd" action="" method="POST">
  <div class="demo-form-row">
	  <label>Name: </label><br>
	  <input type="text" name="name" class="demo-form-field" value="<?php echo $rows['0']['name']; ?>" required  />
  </div>
  <div class="demo-form-row">
	  <label>Email: </label><br>
	  <input name="email" class="demo-form-field"  value="<?php echo $rows['0']['email']; ?>" required />
  </div>
  <div class="demo-form-row">
	  <label>Image: </label><br>
	  <input type="file" name="file" class="demo-form-field" value="<?php echo $rows['0']['file_name']; ?>" required />
  </div>
  <div class="demo-form-row">
	  <input name="save_record" type="submit" value="Save" class="demo-form-submit">
  </div>
  </form>
</div>
</body>
</html>


